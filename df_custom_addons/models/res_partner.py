from odoo import fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    #columns definition
    twitter_account = fields.Char('Twitter Account',size=28)
    facebook_account = fields.Char('Facebook Account',size=28)
    # end of columns definition

#end of res partner